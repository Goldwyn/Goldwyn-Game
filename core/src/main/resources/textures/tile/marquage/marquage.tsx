<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.3" name="marquage" tilewidth="64" tileheight="64" tilecount="2" columns="0">
    <grid orientation="orthogonal" width="1" height="1"/>
    <properties>
        <property name="name" value="not-accessible"/>
    </properties>
    <tile id="0">
        <properties>
            <property name="accessible" type="bool" value="false"/>
        </properties>
        <image width="64" height="64" source="not-accessible.png"/>
    </tile>
    <tile id="1">
        <properties>
            <property name="accessible" type="bool" value="true"/>
        </properties>
        <image width="64" height="64" source="accessible.png"/>
    </tile>
</tileset>
