#version 330

in vec2 textureCoord;
out vec4 fragColor;

uniform vec4 colour;

uniform sampler2D baseTexture;

void main()
{
    fragColor = colour *  texture(baseTexture, textureCoord);
}

