#version 330

in vec2 textureCoord;
out vec4 fragColor;

uniform int hasTexture;
uniform vec4 colour;

uniform sampler2D baseTexture;
uniform sampler2D earsTexture;
uniform sampler2D eyesTexture;
uniform sampler2D noseTexture;

vec4 baseTexel,
earsTexel,
eyesTexel,
noseTexel,
resultColor;

void main()
{
    if (hasTexture == 1){
        baseTexel = texture(baseTexture, textureCoord);
        earsTexel = texture(earsTexture, textureCoord);
        eyesTexel = texture(eyesTexture, textureCoord);
        noseTexel = texture(noseTexture, textureCoord);

        resultColor = mix(baseTexel, earsTexel, earsTexel.a);
        resultColor = mix(resultColor, eyesTexel, eyesTexel.a);
        resultColor = mix(resultColor, noseTexel, noseTexel.a);

        fragColor = colour * resultColor;
    } else {
        fragColor = colour;
    }
}

