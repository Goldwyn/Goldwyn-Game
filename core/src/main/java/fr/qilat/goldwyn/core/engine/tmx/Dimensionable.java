package fr.qilat.goldwyn.core.engine.tmx;

/**
 * Created by Qilat on 2019-03-31 for goldwyn.
 */
public interface Dimensionable {

    void setX(int x);

    void setY(int y);

    void setWidth(int width);

    void setHeight(int height);
}
