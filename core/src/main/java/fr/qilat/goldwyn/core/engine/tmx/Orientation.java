package fr.qilat.goldwyn.core.engine.tmx;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public enum Orientation {
    ORTHOGONAL,
    ISOMETRIC,
    STAGGERED,
    HEXAGONAL
}
