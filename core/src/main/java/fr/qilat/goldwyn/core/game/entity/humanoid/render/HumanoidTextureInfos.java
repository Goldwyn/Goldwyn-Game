package fr.qilat.goldwyn.core.game.entity.humanoid.render;

/**
 * Created by Qilat on 2019-03-29 for goldwyn.
 */
public class HumanoidTextureInfos {

    public static final float FRAME_WIDTH = 1.0f / 13.0f;
    public static final float FRAME_HEIGHT = 1.0f / 21.0f;

}
