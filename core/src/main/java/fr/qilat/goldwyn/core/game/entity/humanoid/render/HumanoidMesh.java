package fr.qilat.goldwyn.core.game.entity.humanoid.render;

import fr.qilat.goldwyn.core.engine.graphic.Material;
import fr.qilat.goldwyn.core.engine.graphic.Mesh;

/**
 * Created by Qilat on 2019-03-28 for goldwyn.
 */
public class HumanoidMesh extends Mesh {

    private static final float[] POSITIONS = {
            -0.5f, 0.5f, 0.0f, // Le To
            -0.5f, -0.5f, 0.0f, // Le Bo
            0.5f, 0.5f, 0.0f, // Ri To
            0.5f, -0.5f, 0.0f // Ri Bo
    };

    private static final int[] INDICES = {
            0, 1, 2,
            3, 2, 1
    };

    public HumanoidMesh() {
        super(POSITIONS, new float[]{}, INDICES);
        this.setMaterial(new Material());
    }
}
