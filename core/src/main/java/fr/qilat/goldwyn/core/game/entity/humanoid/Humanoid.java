package fr.qilat.goldwyn.core.game.entity.humanoid;

import org.dyn4j.collision.CategoryFilter;
import org.dyn4j.dynamics.Body;
import org.dyn4j.dynamics.BodyFixture;
import org.dyn4j.geometry.Geometry;
import org.dyn4j.geometry.MassType;
import org.dyn4j.world.World;

import fr.qilat.goldwyn.core.engine.item.GameItem;
import fr.qilat.goldwyn.core.game.entity.humanoid.animation.AnimationEnum;
import fr.qilat.goldwyn.core.game.entity.humanoid.body.BodySkin;
import fr.qilat.goldwyn.core.game.entity.humanoid.render.HumanoidRenderer;
import lombok.Getter;

/**
 * Created by Qilat on 2019-03-28 for goldwyn.
 */
public class Humanoid extends GameItem {

    @Getter
    private final BodySkin bodySkin;
    @Getter
    private final HumanoidState state;

    public Humanoid() {
        super();
        this.bodySkin = new BodySkin();
        this.state = new HumanoidState(AnimationEnum.Action.STATIC, AnimationEnum.Facing.BOTTOM);
        this.setRenderer(new HumanoidRenderer(this));
    }

    @Override
    public void init(World<Body> world) {
        BodyFixture bf = this.getPhysicBody().addFixture(Geometry.createRectangle(1.0, 2.0));
        bf.setFilter(new CategoryFilter(1, Long.MAX_VALUE));
        this.getPhysicBody().translate(this.getPosition().x, this.getPosition().y);
        this.getPhysicBody().setMass(MassType.NORMAL);

        world.addBody(this.getPhysicBody());
    }

    @Override
    public void input(World<Body> world) {
    }

    @Override
    public void update(World<Body> world, float interval) {
        super.update(world, interval);
    }
}
