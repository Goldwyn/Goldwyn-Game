package fr.qilat.goldwyn.core.game.entity.humanoid.animation;

import lombok.Getter;
import lombok.Setter;
import org.joml.Vector2f;

/**
 * Created by Qilat on 2019-03-29 for goldwyn.
 */
public class Animation {

    @Getter
    @Setter
    private boolean running = false;
    @Getter
    @Setter
    private boolean runningCycle = false;
    @Getter
    private int frameId;

    @Getter
    @Setter
    private AnimationEnum.Action action;
    @Getter
    @Setter
    private AnimationEnum.Facing facing;
    private float intervalSum = 0;

    public Animation(AnimationEnum.Action action, AnimationEnum.Facing facing) {
        this.action = action;
        this.facing = facing;
    }

    public void runOnce() {
        running = true;
    }

    public void runCycle() {
        running = true;
        runningCycle = true;
    }

    public void update(float interval) {
        if (isRunning()) {
            if (this.action.getFrameDuration() > 0.0f) {
                intervalSum += interval;
                frameId = Math.round(intervalSum / action.getFrameDuration()) % action.getFramesAmount();

                if (!isRunningCycle()) {
                    if (frameId == action.getFramesAmount())
                        stop();
                }
            } else {
                frameId = action.getFramesAmount();
            }
        }
    }

    public void stop() {
        running = false;
        frameId = 0;
        intervalSum = 0;
    }

    public Vector2f getFramePos(float frameWidth, float frameHeight) {
        Vector2f coords = new Vector2f();
        coords.x = getFrameId() * frameWidth;
        coords.y = (action.getId() * 4 + facing.getId()) * frameHeight;
        return coords;
    }

}
