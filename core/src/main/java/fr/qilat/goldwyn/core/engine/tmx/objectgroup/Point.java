package fr.qilat.goldwyn.core.engine.tmx.objectgroup;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public class Point extends Shape {

    @Getter
    @Setter
    private int x;
    @Getter
    @Setter
    private int y;

}
