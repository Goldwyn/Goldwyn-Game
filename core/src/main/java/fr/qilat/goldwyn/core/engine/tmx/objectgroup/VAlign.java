package fr.qilat.goldwyn.core.engine.tmx.objectgroup;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public enum VAlign {

    TOP,
    CENTER,
    BOTTOM

}
