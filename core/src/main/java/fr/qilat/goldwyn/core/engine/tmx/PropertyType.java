package fr.qilat.goldwyn.core.engine.tmx;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public enum PropertyType {
    STRING,
    INT,
    FLOAT,
    BOOL,
    COLOR,
    FILE
}
