package fr.qilat.goldwyn.core.engine.tmx.layer;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public enum Encoding {

    CSV,
    BASE64

}
