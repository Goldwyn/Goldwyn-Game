package fr.qilat.goldwyn.core.engine.tmx.tileset.tile;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public class Frame {
    @Getter
    @Setter
    private int tileId;
    @Getter
    @Setter
    private int duration;
}
