package fr.qilat.goldwyn.core.engine.tmx.layer;


import fr.qilat.goldwyn.core.engine.tmx.Dimensionable;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public class Chunk implements Dimensionable {

    @Getter
    @Setter
    ArrayList<Tile> tiles = new ArrayList<>();
    @Getter
    @Setter
    private int x;
    @Getter
    @Setter
    private int y;
    @Getter
    @Setter
    private int width;
    @Getter
    @Setter
    private int height;
}
