package fr.qilat.goldwyn.core.engine.tmx;

import fr.qilat.goldwyn.core.engine.tmx.layer.*;
import fr.qilat.goldwyn.core.engine.tmx.objectgroup.Object;
import fr.qilat.goldwyn.core.engine.tmx.objectgroup.*;
import fr.qilat.goldwyn.core.engine.tmx.tileset.*;
import fr.qilat.goldwyn.core.engine.tmx.tileset.tile.Animation;
import fr.qilat.goldwyn.core.engine.tmx.tileset.tile.Frame;
import fr.qilat.goldwyn.core.engine.tmx.tileset.tile.TilesetTile;
import fr.qilat.goldwyn.core.engine.tmx.tileset.wangset.*;
import lombok.Getter;
import org.apache.commons.lang.StringUtils;
import org.joml.Vector4i;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public class TmxParser {

    @Getter
    private String rootPath;
    @Getter
    private String docName;

    private TmxParser(String pathStr) {
        int lastIndex = pathStr.lastIndexOf('/');
        if (pathStr.indexOf('/') != -1) {
            rootPath = pathStr.substring(0, lastIndex + 1);
            docName = pathStr.substring(lastIndex - 1);
        } else {
            rootPath = "";
            docName = pathStr;
        }
    }

    public static TiledMap createMapFromTmx(String path) {
        try {
            Element rootNode = loadXMLFile(path);
            return new TmxParser(path).createMap(rootNode);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static TileSet createTileSetFromTsx(String path) {
        try {
            Element rootNode = loadXMLFile(path);
            return new TmxParser(path).createTileSet(rootNode);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static Template createTemplateFromXml(String path) {
        try {
            Element rootNode = loadXMLFile(path);
            return new TmxParser(path).createTemplate(rootNode);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static Element loadXMLFile(String path) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new File(path));

        return document.getDocumentElement();
    }

    private static Vector4i getV4iFromHex(Element node, String name, Vector4i defaultV4i) {
        String hexadecimal = node.getAttribute(name);

        if (!hexadecimal.equals("")) {

            if (hexadecimal.charAt(0) == '#')
                hexadecimal = hexadecimal.replace("#", "");

            boolean hasAlpha = hexadecimal.length() == 8;

            String aComponent = hasAlpha ? hexadecimal.substring(0, 2) : "0";
            String rComponent = hexadecimal.substring(hasAlpha ? 2 : 0, hasAlpha ? 4 : 2);
            String gComponent = hexadecimal.substring(hasAlpha ? 4 : 2, hasAlpha ? 6 : 4);
            String bComponent = hexadecimal.substring(hasAlpha ? 6 : 4, hasAlpha ? 8 : 6);

            defaultV4i.w = Integer.parseInt(aComponent, 16);
            defaultV4i.x = Integer.parseInt(rComponent, 16);
            defaultV4i.y = Integer.parseInt(gComponent, 16);
            defaultV4i.z = Integer.parseInt(bComponent, 16);
        }
        return defaultV4i;
    }

    private static Vector4i getV4iFromHex(Element node, String name) {
        return getV4iFromHex(node, name, new Vector4i(0, 0, 0, 0));
    }

    private static String getString(Element node, String attributeName) {
        String value = node.getAttribute(attributeName);
        return (value != null && !value.isEmpty()) ? value : null;
    }

    private static int getInt(Element node, String attributeName, int defaultValue) {
        String value = node.getAttribute(attributeName);
        return (value != null && !value.isEmpty() && StringUtils.isNumeric(value)) ? Integer.parseInt(value) : defaultValue;
    }

    private static float getFloat(Element node, String attributeName, float defaultValue) {
        String value = node.getAttribute(attributeName);
        return (value != null && !value.isEmpty() && StringUtils.isNumeric(value)) ? Float.parseFloat(value) : defaultValue;
    }

    private static boolean getBoolean(Element node, String attributeName, boolean defaultValue) {
        String value = node.getAttribute(attributeName);
        return (value != null && !value.isEmpty()) ? value.equals("1") : defaultValue;
    }

    private TiledMap createMap(Element node) {
        TiledMap map = new TiledMap();
        map.setVersion(getString(node, "version"));
        map.setTiledVersion(getString(node, "tiledversion"));
        map.setOrientation(Orientation.valueOf(node.getAttribute("orientation").toUpperCase()));
        map.setRenderOrder(Arrays.stream(RenderOrder.values()).filter(renderOrder -> renderOrder.getXmlValue().equals(node.getAttribute("renderorder"))).findFirst().orElse(RenderOrder.RIGHTDOWN));
        map.setWidth(getInt(node, "width", 0));
        map.setHeight(getInt(node, "height", 0));
        map.setTileWidth(getInt(node, "tilewidth", 0));
        map.setTileHeight(getInt(node, "tileheight", 0));
        map.setBackgroundColor(getV4iFromHex(node, "backgroundcolor"));
        map.setNextLayerId(getInt(node, "nextlayerid", 0));
        map.setNextObjectId(getInt(node, "nextobjectid", 0));

        if (map.getOrientation().equals(Orientation.STAGGERED) || map.getOrientation().equals(Orientation.HEXAGONAL)) {
            map.setStaggerAxis(StaggerAxis.valueOf(node.getAttribute("staggeraxis").toUpperCase()));
            map.setStaggerIndex(node.getAttribute("staggerindex"));
            if (map.getOrientation().equals(Orientation.HEXAGONAL))
                map.setHexSideLength(getInt(node, "hexsidelength", 0));
        }

        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            if (children.item(i).getNodeType() == Node.ELEMENT_NODE) {
                Element child = (Element) children.item(i);
                switch (child.getNodeName()) {
                    case "properties":
                        map.setProperties(createProperties(child));
                        break;
                    case "tileset":
                        map.getTileSets().add(createTileSet(child));
                        break;
                    case "layer":
                        map.getLayers().add(createTileLayer(child));
                        break;
                    case "objectgroup":
                        map.getLayers().add(createObjectGroup(child));
                        break;
                    case "imagelayer":
                        map.getLayers().add(createImageLayer(child));
                        break;
                    case "group":
                        map.getGroups().add(createGroup(child));
                        break;
                }
            }
        }
        return map;
    }

    private void createLayer(Element node, Layer layer) {
        layer.setId(getInt(node, "id", 0));
        layer.setName(getString(node, "name"));
        layer.setOffsetX(getInt(node, "offsetx", 0));
        layer.setOffsetY(getInt(node, "offsety", 0));
        layer.setOpacity(getFloat(node, "opacity", 1.0f));
        layer.setVisible(getBoolean(node, "visible", true));

        NodeList children = node.getElementsByTagName("properties");
        for (int i = 0; i < children.getLength(); i++) {
            if (children.item(i).getNodeType() == Node.ELEMENT_NODE) {
                layer.setProperties(createProperties((Element) children.item(i)));
            }
        }
    }

    private Group createGroup(Element node) {
        Group group = new Group();
        createLayer(node, group);

        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            if (children.item(i).getNodeType() == Node.ELEMENT_NODE) {
                Element child = (Element) children.item(i);
                switch (child.getNodeName()) {
                    case "layer":
                        group.getLayers().add(createTileLayer(child));
                        break;
                    case "objectgroup":
                        group.getLayers().add(createObjectGroup(child));
                        break;
                    case "imagelayer":
                        group.getLayers().add(createImageLayer(child));
                        break;
                    case "group":
                        group.getGroups().add(createGroup(child));
                        break;
                }
            }
        }
        return group;
    }

    private ImageLayer createImageLayer(Element node) {
        ImageLayer imageLayer = new ImageLayer();
        createLayer(node, imageLayer);

        NodeList children = node.getElementsByTagName("image");
        for (int i = 0; i < children.getLength(); i++) {
            if (children.item(i).getNodeType() == Node.ELEMENT_NODE) {
                imageLayer.setImage(createImage((Element) children.item(i)));
            }
        }
        return imageLayer;
    }

    private void createDimensionable(Element node, Dimensionable dimensionable) {
        dimensionable.setX(getInt(node, "x", 0));
        dimensionable.setY(getInt(node, "y", 0));
        dimensionable.setWidth(getInt(node, "width", 0));
        dimensionable.setHeight(getInt(node, "height", 0));
    }

    private TileLayer createTileLayer(Element node) {
        TileLayer tileLayer = new TileLayer();
        createLayer(node, tileLayer);
        createDimensionable(node, tileLayer);
        NodeList children = node.getElementsByTagName("data");
        for (int i = 0; i < children.getLength(); i++) {
            if (children.item(i).getNodeType() == Node.ELEMENT_NODE) {
                tileLayer.setData(createData((Element) children.item(i)));
            }
        }
        return tileLayer;
    }

    private TileSet createTileSet(Element node) {
        TileSet tileSet = new TileSet();
        tileSet.setSource(getString(node, "source"));

        if (tileSet.getSource() != null) {
            tileSet = createTileSetFromTsx(this.rootPath + tileSet.getSource());
            if (tileSet != null)
                tileSet.setFirstgid(getInt(node, "firstgid", 1));
            return tileSet;
        }

        tileSet.setFirstgid(getInt(node, "firstgid", 1));
        tileSet.setName(getString(node, "name"));
        tileSet.setTileWidth(getInt(node, "tilewidth", 0));
        tileSet.setTileHeight(getInt(node, "tileheight", 0));
        tileSet.setSpacing(getInt(node, "spacing", 0));
        tileSet.setTilecount(getInt(node, "tilecount", 0));
        tileSet.setColumns(getInt(node, "columns", 0));

        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            if (children.item(i).getNodeType() == Node.ELEMENT_NODE) {

                Element child = (Element) children.item(i);
                switch (child.getNodeName()) {
                    case "properties":
                        tileSet.setProperties(createProperties(child));
                        break;
                    case "tileoffset":
                        tileSet.setTileOffset(createTileOffset(child));
                        break;
                    case "grid":
                        tileSet.setGrid(createGrid(child));
                        break;
                    case "image":
                        tileSet.getImages().add(createImage(child));
                        break;
                    case "terraintypes":
                        tileSet.getTerraintypes().add(createTerrainTypes(child));
                        break;
                    case "tile":
                        tileSet.getTilesetTiles().add(createTilesetTile(child));
                        break;
                    case "wangsets":
                        tileSet.setWangsets(createWangsets(child));
                        break;
                }
            }
        }
        return tileSet;
    }

    private Wangsets createWangsets(Element node) {
        Wangsets wangsets = new Wangsets();
        NodeList children = node.getElementsByTagName("wangset");
        for (int i = 0; i < children.getLength(); i++) {
            if (children.item(i).getNodeType() == Node.ELEMENT_NODE) {
                Element child = (Element) children.item(i);
                wangsets.getWangsets().add(createWangset(child));
            }
        }
        return wangsets;
    }

    private Wangset createWangset(Element node) {
        Wangset wangset = new Wangset();
        wangset.setName(node.getAttribute("name"));
        wangset.setTile(getInt(node, "tile", 0));

        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            if (children.item(i).getNodeType() == Node.ELEMENT_NODE) {
                Element child = (Element) children.item(i);
                switch (child.getNodeName()) {
                    case "wangcornercolor":
                        wangset.getWangCornerColors().add(createWangCornerColor(child));
                        break;
                    case "wangedgecolor":
                        wangset.getWangEdgeColors().add(createWandEdgeColor(child));
                        break;
                    case "wangtile":
                        wangset.getWangTiles().add(createWangTile(child));
                        break;
                }
            }
        }
        return wangset;
    }

    private WangTile createWangTile(Element node) {
        WangTile wangTile = new WangTile();
        wangTile.setTileId(getInt(node, "tileid", 0));
        wangTile.setWangId(getString(node, "wangid"));
        return wangTile;
    }

    private void createWangBorderColor(Element node, WangBorderColor wangBorderColor) {
        wangBorderColor.setName(getString(node, "name"));
        wangBorderColor.setColor(getV4iFromHex(node, "color"));
        wangBorderColor.setTile(getInt(node, "tile", 0));
        wangBorderColor.setProbability(getFloat(node, "probability", 0.0f));
    }

    private WangEdgeColor createWandEdgeColor(Element node) {
        WangEdgeColor wangEdgeColor = new WangEdgeColor();
        createWangBorderColor(node, wangEdgeColor);
        return wangEdgeColor;
    }

    private WangCornerColor createWangCornerColor(Element node) {
        WangCornerColor wangCornerColor = new WangCornerColor();
        createWangBorderColor(node, wangCornerColor);
        return wangCornerColor;
    }

    private TilesetTile createTilesetTile(Element node) {
        TilesetTile tilesetTile = new TilesetTile();
        tilesetTile.setId(getInt(node, "id", 0));
        tilesetTile.setType(getString(node, "type"));
        tilesetTile.setTerrain(getString(node, "terrain"));
        tilesetTile.setProbability(getFloat(node, "probability", 0.0f));

        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            if (children.item(i).getNodeType() == Node.ELEMENT_NODE) {
                Element child = (Element) children.item(i);
                switch (child.getNodeName()) {
                    case "properties":
                        tilesetTile.setProperties(createProperties(child));
                    case "image":
                        tilesetTile.setImage(createImage(child));
                        break;
                    case "objectgroup":
                        tilesetTile.setObjectGroup(createObjectGroup(child));
                        break;
                    case "animation":
                        tilesetTile.setAnimation(createAnimation(child));
                        break;
                }
            }
        }
        return tilesetTile;
    }

    private ObjectGroup createObjectGroup(Element node) {
        ObjectGroup objectGroup = new ObjectGroup();
        createLayer(node, objectGroup);

        objectGroup.setColor(getV4iFromHex(node, "color"));

        String drawOrderValue = getString(node, "draworder");
        if (drawOrderValue != null) {
            objectGroup.setDrawOrder(DrawOrder.valueOf(drawOrderValue.toUpperCase()));
        } else {
            objectGroup.setDrawOrder(DrawOrder.TOPDOWN);
        }

        NodeList children = node.getElementsByTagName("object");
        for (int i = 0; i < children.getLength(); i++) {
            if (children.item(i).getNodeType() == Node.ELEMENT_NODE) {
                Element child = (Element) children.item(i);
                objectGroup.getObjects().add(createObject(child));
            }
        }
        return objectGroup;
    }

    @SuppressWarnings("Duplicates")
    private Object createObject(Element node) {
        Object object = new Object();
        String templatePath = getString(node, "template");
        if (templatePath != null) {
            Template template = createTemplateFromXml(this.rootPath + templatePath);
            object.setTemplate(template);
            object = new Object(object.getTemplate().getObject());
            object.setTemplate(template);
        }

        object.setId(getInt(node, "id", 0));
        object.setName(node.getAttribute("name"));
        object.setType(node.getAttribute("type"));

        createDimensionable(node, object);

        object.setRotation(getFloat(node, "rotation", 0.0f));
        object.setGid(getInt(node, "gid", 0));
        object.setVisible(getBoolean(node, "visible", true));

        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            if (children.item(i).getNodeType() == Node.ELEMENT_NODE) {

                Element child = (Element) children.item(i);
                switch (child.getNodeName()) {
                    case "properties":
                        object.setProperties(createProperties(child));
                        break;
                    case "ellipse":
                        object.setShape(createEllipse(child));
                        break;
                    case "polygon":
                        object.setShape(createPolygon(child));
                        break;
                    case "polyline":
                        object.setShape(createPolyline(child));
                        break;
                    case "text":
                        object.setShape(createText(child));
                        break;
                    case "image":
                        object.setShape(createImage(child));
                        break;
                }
            }
        }
        return object;
    }

    private Text createText(Element node) {
        Text text = new Text();
        text.setFontFamily(node.getAttribute("fontfamily"));
        text.setPixelSize(getInt(node, "pixelsize", 0));
        text.setWrap(getBoolean(node, "wrap", false));
        text.setColor(getV4iFromHex(node, "color"));
        text.setBold(getBoolean(node, "bold", false));
        text.setItalic(getBoolean(node, "italic", false));
        text.setUnderline(getBoolean(node, "underline", false));
        text.setStrikeout(getBoolean(node, "strikeout", false));
        text.setKerning(getBoolean(node, "kerning", false));

        text.setHAlign(HAlign.valueOf(node.getAttribute("halign").toUpperCase()));
        text.setVAlign(VAlign.valueOf(node.getAttribute("valign").toUpperCase()));

        text.setContent(node.getTextContent());
        return text;
    }

    private Polyline createPolyline(Element node) {
        Polyline polyline = new Polyline();
        NodeList children = node.getElementsByTagName("points");
        for (int i = 0; i < children.getLength(); i++) {
            if (children.item(i).getNodeType() == Node.ELEMENT_NODE) {
                Element child = (Element) children.item(i);
                polyline.getPoints().add(createPoint(child));
            }
        }
        return polyline;
    }

    private Polygon createPolygon(Element node) {
        Polygon polygon = new Polygon();

        NodeList children = node.getElementsByTagName("points");
        for (int i = 0; i < children.getLength(); i++) {
            if (children.item(i).getNodeType() == Node.ELEMENT_NODE) {
                Element child = (Element) children.item(i);
                polygon.getPoints().add(createPoint(child));
            }
        }
        return polygon;
    }

    private Point createPoint(Element node) {
        Point point = new Point();
        point.setX(getInt(node, "x", 0));
        point.setY(getInt(node, "y", 0));
        return point;

    }

    private Ellipse createEllipse(Element node) {
        Ellipse ellipse = new Ellipse();
        createDimensionable(node, ellipse);
        return ellipse;
    }

    private Template createTemplate(Element node) {
        Template template = new Template();
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Element child = (Element) children.item(i);
            switch (child.getNodeName()) {
                case "tileset":
                    template.getTileSets().add(createTileSet(child));
                    break;
                case "object":
                    template.setObject(createObject(child));
                    break;
            }
        }
        return template;
    }

    private Animation createAnimation(Element node) {
        Animation animation = new Animation();

        NodeList children = node.getElementsByTagName("frame");
        for (int i = 0; i < children.getLength(); i++) {
            if (children.item(i).getNodeType() == Node.ELEMENT_NODE) {
                Element child = (Element) children.item(i);
                animation.getFrames().add(createFrame(child));
            }
        }
        return animation;
    }

    private Frame createFrame(Element node) {
        Frame frame = new Frame();
        frame.setTileId(getInt(node, "tileid", 0));
        frame.setDuration(getInt(node, "duration", 0));
        return frame;
    }

    private TerrainTypes createTerrainTypes(Element node) {
        TerrainTypes terrainTypes = new TerrainTypes();

        NodeList children = node.getElementsByTagName("terrain");
        for (int i = 0; i < children.getLength(); i++) {
            if (children.item(i).getNodeType() == Node.ELEMENT_NODE) {
                Element child = (Element) children.item(i);
                terrainTypes.getTerrains().add(createTerrain(child));
            }
        }
        return terrainTypes;
    }

    private Terrain createTerrain(Element node) {
        Terrain terrain = new Terrain();
        terrain.setName(getString(node, "name"));
        terrain.setTile(getInt(node, "tile", 0));

        NodeList children = node.getElementsByTagName("properties");
        for (int i = 0; i < children.getLength(); i++) {
            if (children.item(i).getNodeType() == Node.ELEMENT_NODE) {
                Element child = (Element) children.item(i);
                terrain.setProperties(createProperties(child));
            }
        }
        return terrain;
    }

    private Image createImage(Element node) {
        Image image = new Image();
        image.setFormat(node.getAttribute("format"));
        image.setId(getInt(node, "id", 0));
        image.setSource("/" + this.getRootPath() + node.getAttribute("source"));
        image.setTrans(getV4iFromHex(node, "trans"));
        image.setWidth(getInt(node, "width", 0));
        image.setHeight(getInt(node, "height", 0));

        NodeList children = node.getElementsByTagName("data");
        for (int i = 0; i < children.getLength(); i++) {
            if (children.item(i).getNodeType() == Node.ELEMENT_NODE) {
                Element child = (Element) children.item(i);
                image.setData(createData(child));
            }
        }
        return image;
    }

    private Data createData(Element node) {
        Data data = new Data();
        data.setCompression(Compression.valueOf(node.getAttribute("compression").toUpperCase()));
        data.setEncoding(Encoding.valueOf(node.getAttribute("encoding").toUpperCase()));

        NodeList tileList = node.getElementsByTagName("tile");
        NodeList chunkList = node.getElementsByTagName("chunk");
        if (tileList.getLength() > 0 || chunkList.getLength() > 0) {
            for (int i = 0; i < tileList.getLength(); i++) {
                if (tileList.item(i).getNodeType() == Node.ELEMENT_NODE) {
                    Element child = (Element) tileList.item(i);
                    data.getTiles().add(createTile(child));
                }
            }
            for (int i = 0; i < chunkList.getLength(); i++) {
                if (chunkList.item(i).getNodeType() == Node.ELEMENT_NODE) {
                    Element child = (Element) chunkList.item(i);
                    data.getChunks().add(createChunk(child));
                }
            }
        } else {
            data.setRawContent(node.getTextContent());
        }
        return data;
    }

    private Chunk createChunk(Element node) {
        Chunk chunk = new Chunk();
        createDimensionable(node, chunk);

        NodeList children = node.getElementsByTagName("tile");
        for (int i = 0; i < children.getLength(); i++) {
            if (children.item(i).getNodeType() == Node.ELEMENT_NODE) {
                Element child = (Element) children.item(i);
                chunk.getTiles().add(createTile(child));
            }
        }
        return chunk;
    }

    private Tile createTile(Element node) {
        Tile tile = new Tile();
        tile.setGid(getInt(node, "gid", 0));
        tile.setContent(node.getTextContent());
        return tile;
    }

    private Grid createGrid(Element node) {
        Grid grid = new Grid();
        grid.setOrientation(Orientation.valueOf(node.getAttribute("orientation").toUpperCase()));
        grid.setWidth(getInt(node, "width", 0));
        grid.setHeight(getInt(node, "height", 0));
        return grid;
    }

    private TileOffset createTileOffset(Element node) {
        TileOffset tileOffset = new TileOffset();
        tileOffset.setX(getInt(node, "x", 0));
        tileOffset.setY(getInt(node, "y", 0));
        return tileOffset;
    }

    private Property createProperty(Element node) {
        Property property = new Property();
        property.setName(node.getAttribute("name"));
        property.setType(PropertyType.valueOf(node.getAttribute("type").toUpperCase()));
        property.setValue(node.getAttribute("value"));
        return property;
    }

    private Properties createProperties(Element node) {
        Properties properties = new Properties();

        NodeList children = node.getElementsByTagName("property");
        for (int i = 0; i < children.getLength(); i++) {
            if (children.item(i).getNodeType() == Node.ELEMENT_NODE) {
                Element child = (Element) children.item(i);
                Property property = createProperty(child);
                properties.add(property.getName(), property);
            }
        }
        return properties;
    }

}
