package fr.qilat.goldwyn.core.engine.graphic;

import fr.qilat.goldwyn.core.engine.IHud;
import fr.qilat.goldwyn.core.engine.Scene;
import fr.qilat.goldwyn.core.engine.Window;

/**
 * Created by Qilat on 2019-03-29 for goldwyn.
 */
public interface IRenderer {

    void init(Window window) throws Exception;

    void render(Window window, Camera camera, Scene scene, IHud hud);

    void cleanup();

}
