package fr.qilat.goldwyn.core.engine.tmx;

import fr.qilat.goldwyn.core.engine.tmx.objectgroup.Object;
import fr.qilat.goldwyn.core.engine.tmx.tileset.TileSet;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public class Template {
    @Getter
    @Setter
    private ArrayList<TileSet> tileSets = new ArrayList<>();
    @Getter
    @Setter
    private Object object;

}
