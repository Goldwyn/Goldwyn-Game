package fr.qilat.goldwyn.core.engine.input;

import lombok.Getter;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import org.lwjgl.glfw.GLFWScrollCallback;

import java.util.Arrays;

import static org.lwjgl.glfw.GLFW.*;

/**
 * Created by Qilat on 2019-03-27 for goldwyn.
 */
public class Input {

    private static final int KEYBOARD_SIZE = 512;
    private static final int MOUSE_SIZE = 16;

    private static int[] keyStates = new int[KEYBOARD_SIZE];
    private static boolean[] activeKeys = new boolean[KEYBOARD_SIZE];
    public static GLFWKeyCallback keyboard = new GLFWKeyCallback() {
        @Override
        public void invoke(long window, int key, int scancode, int action, int mods) {
            if (key != -1) {
                activeKeys[key] = action != GLFW_RELEASE;
                keyStates[key] = action;
            }
        }
    };
    private static int[] mouseButtonStates = new int[MOUSE_SIZE];
    private static boolean[] activeMouseButtons = new boolean[MOUSE_SIZE];
    public static GLFWMouseButtonCallback mouse = new GLFWMouseButtonCallback() {
        @Override
        public void invoke(long window, int button, int action, int mods) {
            if (button != -1) {
                activeMouseButtons[button] = action != GLFW_RELEASE;
                mouseButtonStates[button] = action;
            }
        }
    };
    private static long lastMouseNS = 0;
    private static long mouseDoubleClickPeriodNS = 1000000000 / 5; //5th of a second for double click.
    private static int NO_STATE = -1;
    @Getter
    private static double lastXOffset = 0D;
    @Getter
    private static double lastYOffset = 0D;
    public static GLFWScrollCallback scroll = new GLFWScrollCallback() {
        @Override
        public void invoke(long window, double xoffset, double yoffset) {
            lastXOffset += xoffset;
            lastYOffset += yoffset;
        }
    };

    public static void update() {
        resetKeyboard();
        resetMouse();
        resetScroll();

        glfwPollEvents();
    }

    private static void resetKeyboard() {
        Arrays.fill(keyStates, NO_STATE);
    }

    private static void resetMouse() {
        Arrays.fill(mouseButtonStates, NO_STATE);

        long now = System.nanoTime();

        if (now - lastMouseNS > mouseDoubleClickPeriodNS)
            lastMouseNS = 0;
    }

    private static void resetScroll() {
        lastXOffset = 0;
        lastYOffset = 0;
    }

    public static boolean keyDown(int key) {
        return activeKeys[key];
    }

    public static boolean keyPressed(int key) {
        return keyStates[key] == GLFW_PRESS;
    }

    public static boolean keyReleased(int key) {
        return keyStates[key] == GLFW_RELEASE;
    }

    public static boolean mouseButtonDown(int button) {
        return activeMouseButtons[button];
    }

    public static boolean mouseButtonPressed(int button) {
        return mouseButtonStates[button] == GLFW_RELEASE;
    }

    public static boolean mouseButtonReleased(int button) {
        boolean flag = mouseButtonStates[button] == GLFW_RELEASE;

        if (flag)
            lastMouseNS = System.nanoTime();

        return flag;
    }

    public static boolean mouseButtonDoubleClicked(int button) {
        long last = lastMouseNS;
        boolean flag = mouseButtonReleased(button);

        long now = System.nanoTime();

        if (flag && now - last < mouseDoubleClickPeriodNS) {
            lastMouseNS = 0;
            return true;
        }
        return false;
    }
}
