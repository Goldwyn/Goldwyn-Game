package fr.qilat.goldwyn.core.engine.item;

import org.dyn4j.collision.CategoryFilter;
import org.dyn4j.dynamics.Body;
import org.dyn4j.dynamics.BodyFixture;
import org.dyn4j.geometry.Geometry;
import org.dyn4j.geometry.MassType;
import org.dyn4j.geometry.Vector2;
import org.dyn4j.world.World;

public class WorldBorder extends GameItem {

    private final boolean vertical;
    private final float length;
    private final Vector2 position;

    public WorldBorder(boolean vertical, float length, Vector2 position){
        this.setVisible(false);
        this.vertical = vertical;
        this.length = length;
        this.position = position;
    }

    @Override
    public void init(World<Body> world) {
        BodyFixture bf = this.getPhysicBody().addFixture(Geometry.createRectangle(this.vertical ? 1.0 : this.length, this.vertical ? this.length : 1.0));
        bf.setFilter(new CategoryFilter(1, Long.MAX_VALUE));
        this.getPhysicBody().translate(this.position);
        this.getPhysicBody().setMass(MassType.INFINITE);

        world.addBody(this.getPhysicBody());
    }

    @Override
    public void input(World<Body> world) {

    }
}