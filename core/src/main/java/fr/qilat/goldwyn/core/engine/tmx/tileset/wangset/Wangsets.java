package fr.qilat.goldwyn.core.engine.tmx.tileset.wangset;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public class Wangsets {

    @Getter
    @Setter
    private ArrayList<Wangset> wangsets = new ArrayList<>();

}
