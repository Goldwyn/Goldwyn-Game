package fr.qilat.goldwyn.core.engine.tmx.tileset;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public class TerrainTypes {
    @Getter
    @Setter
    private ArrayList<Terrain> terrains = new ArrayList<>();

}
