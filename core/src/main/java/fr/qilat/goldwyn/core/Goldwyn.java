package fr.qilat.goldwyn.core;

import fr.qilat.goldwyn.core.engine.AssetsManager;
import fr.qilat.goldwyn.core.engine.GameEngine;
import fr.qilat.goldwyn.core.engine.IGameLogic;
import fr.qilat.goldwyn.core.game.stage.MappedStage;
import lombok.Getter;

/**
 * Created by Qilat on 2019-03-27 for goldwyn.
 */
public class Goldwyn {

    @Getter
    private static GameEngine gameEngine;

    public static void main(String[] args) {
        try {
            boolean vSync = false;
            boolean resizable = true;
            IGameLogic gameLogic = new MappedStage();
            gameEngine = new GameEngine("Goldwyn Project", 1080, 720, vSync, resizable, gameLogic);
            gameEngine.start();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        } finally {
            AssetsManager.get().cleanUp();
        }
    }

}

