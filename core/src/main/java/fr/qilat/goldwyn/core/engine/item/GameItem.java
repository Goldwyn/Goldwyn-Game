package fr.qilat.goldwyn.core.engine.item;

import lombok.Getter;
import lombok.Setter;

import org.dyn4j.dynamics.Body;
import org.dyn4j.geometry.Vector2;
import org.dyn4j.world.World;
import org.joml.Vector3f;

/**
 * Created by Theophile on 2018-12-09 for projecty.
 */
public abstract class GameItem {

    @Getter
    private final Vector3f position;
    @Getter
    private final Vector3f rotation;
    @Getter
    @Setter
    private GameItemRenderer renderer;
    @Getter
    @Setter
    private float scale;
    @Getter
    @Setter
    private boolean visible;

    @Getter
    private final Body physicBody;

    public GameItem() {
        this.physicBody = new Body();
        this.position = new Vector3f(0, 0, 0);
        this.rotation = new Vector3f(0, 0, 0);
        this.scale = 1;
        this.visible = true;
    }

    public GameItem(GameItemRenderer renderer) {
        this();
        this.renderer = renderer;
    }

    public abstract void init(World<Body> world);

    public abstract void input(World<Body> world);

    public void update(World<Body> world, float interval) {
        Vector2 vector2 = this.physicBody.getWorldCenter();
        this.setPositiond(vector2.x, vector2.y, 0.0);
    };

    private void setPositiond(double x, double y, double z) {
        this.position.x = (float) x;
        this.position.y = (float) y;
        this.position.z = (float) z;
    }
    
    //todo wont work
    public void setPosition(Vector3f vector3f) {
        this.position.x = vector3f.x;
        this.position.y = vector3f.y;
        this.position.z = vector3f.z;
    }

}
