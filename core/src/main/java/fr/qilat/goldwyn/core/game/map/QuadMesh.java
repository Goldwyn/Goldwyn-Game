package fr.qilat.goldwyn.core.game.map;

import fr.qilat.goldwyn.core.engine.graphic.Material;
import fr.qilat.goldwyn.core.engine.graphic.Mesh;

/**
 * Created by Qilat on 2019-03-31 for goldwyn.
 */
public class QuadMesh extends Mesh {

    private static final float[] POSITIONS = {
            -0.5f, 0.5f, 0.0f, // Le To
            -0.5f, -0.5f, 0.0f, // Le Bo
            0.5f, 0.5f, 0.0f, // Ri To
            0.5f, -0.5f, 0.0f // Ri Bo
    };

    private static final float[] TEXTURE = {
            0.0f, 0.0f, // Le To
            1.0f, 0.0f,
            0.0f, 1.0f,
            1.0f, 1.0f,
    };

    private static final int[] INDICES = {
            0, 1, 2,
            3, 2, 1
    };

    public QuadMesh() {
        super(POSITIONS, TEXTURE, INDICES);
        this.setMaterial(new Material());
    }
}
