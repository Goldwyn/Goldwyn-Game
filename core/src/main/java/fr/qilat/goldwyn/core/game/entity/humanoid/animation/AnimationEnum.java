package fr.qilat.goldwyn.core.game.entity.humanoid.animation;

import lombok.Getter;

/**
 * Created by Qilat on 2019-03-29 for goldwyn.
 */
public class AnimationEnum {

    public enum Action {
        STATIC(2, 0, 0.0f),
        SPELLCAST(0, 7, 0.15f),
        THRUST(1, 8, 0.0f),
        RUN(2, 9, 0.05f),
        WALK(2, 9, 0.15f),
        SLASH(3, 6, 0.0f),
        SHOOT(4, 13, 0.0f),
        HURT(5, 6, 0.0f);

        @Getter
        private int id;
        @Getter
        private int framesAmount;
        @Getter
        private float frameDuration;

        Action(int id, int framesAmount, float frameDuration) {
            this.id = id;
            this.framesAmount = framesAmount;
            this.frameDuration = frameDuration;
        }
    }


    public enum Facing {
        TOP(0),
        LEFT(1),
        BOTTOM(2),
        RIGHT(3);

        @Getter
        private int id;

        Facing(int id) {
            this.id = id;
        }
    }

}
