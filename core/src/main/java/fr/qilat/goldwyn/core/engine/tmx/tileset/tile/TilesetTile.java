package fr.qilat.goldwyn.core.engine.tmx.tileset.tile;

import fr.qilat.goldwyn.core.engine.tmx.Properties;
import fr.qilat.goldwyn.core.engine.tmx.objectgroup.ObjectGroup;
import fr.qilat.goldwyn.core.engine.tmx.tileset.Image;
import fr.qilat.goldwyn.core.game.map.QuadMesh;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public class TilesetTile {

    @Getter
    @Setter
    private int id;
    @Getter
    @Setter
    private String type;
    @Getter
    @Setter
    private String terrain;
    @Getter
    @Setter
    private float probability;

    @Getter
    @Setter
    private Properties properties;
    @Getter
    @Setter
    private Image image;
    @Getter
    @Setter
    private ObjectGroup objectGroup;
    @Getter
    @Setter
    private Animation animation;

    @Getter
    @Setter
    private QuadMesh mesh;
}
