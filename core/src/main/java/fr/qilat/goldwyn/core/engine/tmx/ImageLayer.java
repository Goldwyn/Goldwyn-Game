package fr.qilat.goldwyn.core.engine.tmx;

import fr.qilat.goldwyn.core.engine.tmx.tileset.Image;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public class ImageLayer extends Layer {

    @Getter
    @Setter
    private Image image;

}
