package fr.qilat.goldwyn.core.game.stage;

import fr.qilat.goldwyn.core.engine.IHud;
import fr.qilat.goldwyn.core.engine.Scene;
import fr.qilat.goldwyn.core.engine.Window;
import fr.qilat.goldwyn.core.engine.graphic.Camera;
import fr.qilat.goldwyn.core.engine.graphic.IRenderer;
import fr.qilat.goldwyn.core.engine.graphic.Transformation;
import fr.qilat.goldwyn.core.engine.item.GameItem;
import fr.qilat.goldwyn.core.game.entity.humanoid.Humanoid;
import fr.qilat.goldwyn.core.game.entity.humanoid.render.HumanoidShader;

import static org.lwjgl.opengl.GL11.*;

import java.util.ArrayList;
import java.util.List;

public class MappedStageRenderer implements IRenderer {

    private static final float FOV = (float) Math.toRadians(60.0f);
    private static final float Z_NEAR = 0.01f;
    private static final float Z_FAR = 1000.f;

    private final Transformation transformation;
    private HumanoidShader humanoidShader;
    private MapShader mapShader;

    public MappedStageRenderer() {
        transformation = new Transformation();
    }

    @Override
    public void init(Window window) throws Exception {
        humanoidShader = new HumanoidShader();
        mapShader = new MapShader();
    }

    private void clear() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    @Override
    public void render(Window window, Camera camera, Scene scene, IHud hud) {
        clear();

        if (window.isResized()) {
            glViewport(0, 0, window.getWidth(), window.getHeight());
            window.setResized(false);
        }

        transformation.updateProjectionMatrix(FOV, window.getWidth(), window.getHeight(), Z_NEAR, Z_FAR);
        transformation.updateViewMatrix(camera);

        mapShader.render(transformation, scene.getTiledMap());

        //TODO improve this
        List<Humanoid> humanoids = new ArrayList<>();
        for(GameItem item : scene.getGameItems()){
            if(item instanceof Humanoid)
                humanoids.add( (Humanoid) item);
        }
        
        humanoidShader.render(transformation, humanoids);
    }

    @Override
    public void cleanup() {
        if (humanoidShader != null)
            humanoidShader.cleanup();
    }
}
