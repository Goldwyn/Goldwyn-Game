package fr.qilat.goldwyn.core.engine.graphic;

import lombok.Getter;
import org.joml.Vector3f;

public class Camera {

    public static final float CAMERA_MIN_Z = 10f;
    public static final float CAMERA_MAX_Z = 35f;

    @Getter
    private final Vector3f position;

    public Camera() {
        position = new Vector3f(0, 0, 10);
    }

    public Camera(Vector3f position) {
        this.position = position;
    }

    public void setPosition(float x, float y, float z) {
        position.x = x;
        position.y = y;
        position.z = z;
    }

    public void movePosition(float offsetX, float offsetY, float offsetZ) {
        position.x += offsetX;
        position.y += offsetY;
        position.z += offsetZ;
    }

    public void clampPosition(float x, float y, float z){
        position.x = x;
        position.y = y;

        position.z = (z > CAMERA_MAX_Z ? CAMERA_MAX_Z : (z < CAMERA_MIN_Z ? CAMERA_MIN_Z : z));

    }
}
