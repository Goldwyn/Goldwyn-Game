package fr.qilat.goldwyn.core.engine.tmx.tileset.wangset;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public class Wangset {

    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private int tile;

    @Getter
    @Setter
    private ArrayList<WangCornerColor> wangCornerColors = new ArrayList<>();
    @Getter
    @Setter
    private ArrayList<WangEdgeColor> wangEdgeColors = new ArrayList<>();
    @Getter
    @Setter
    private ArrayList<WangTile> wangTiles = new ArrayList<>();


}
