package fr.qilat.goldwyn.core.engine;

import fr.qilat.goldwyn.core.engine.graphic.Texture;
import fr.qilat.goldwyn.core.engine.tmx.*;
import fr.qilat.goldwyn.core.engine.tmx.layer.*;
import fr.qilat.goldwyn.core.engine.tmx.objectgroup.ObjectGroup;
import fr.qilat.goldwyn.core.engine.tmx.tileset.TileSet;
import fr.qilat.goldwyn.core.engine.tmx.tileset.tile.TilesetTile;
import fr.qilat.goldwyn.core.game.map.QuadMesh;
import fr.qilat.goldwyn.core.game.map.QuadRenderer;
import org.joml.Vector3f;

import jakarta.xml.bind.DatatypeConverter;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.zip.InflaterInputStream;

/**
 * Created by Qilat on 2019-03-28 for goldwyn.
 */
public class AssetsManager {

    private static AssetsManager instance = new AssetsManager();

    private static final int FLIPPED_HORIZONTALLY_FLAG = 0x80000000;

    private HashMap<String, Texture> textures;
    private static final int FLIPPED_VERTICALLY_FLAG = 0x40000000;
    private static final int FLIPPED_DIAGONALLY_FLAG = 0x20000000;

    public Texture getTexture(String path) {
        try {
            Texture texture = textures.get(path);
            if (texture != null)
                return texture;
            textures.put(path, texture = new Texture(path));
            return texture;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public void deleteTexture(String path) {
        Texture texture = textures.remove(path);
        if (texture != null)
            texture.cleanup();
    }

    private HashMap<String, TiledMap> tiledMaps;

    private AssetsManager() {
        this.textures = new HashMap<>();
        this.tiledMaps = new HashMap<>();
    }

    public static AssetsManager get() {
        return instance;
    }

    public static Vector3f convertMapCoordToInGameCoord(Vector3f mapCoord) {
        return new Vector3f(mapCoord.x, -mapCoord.y, mapCoord.z);
    }

    public static Vector3f convertInGameCoordToMapCoord(Vector3f igCoord) {
        return new Vector3f(igCoord.x, -igCoord.y, igCoord.z);
    }

    private static byte[] decompressZlib(byte[] bytes, int dataSize) throws IOException {
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
        InflaterInputStream iis = new InflaterInputStream(bais);

        byte[] buf = new byte[dataSize];
        if (iis.read(buf) != -1)
            return buf;

        return new byte[0];
    }

    public TiledMap getTiledMap(String path) {
        try {
            TiledMap map = tiledMaps.get(path);
            if (map != null)
                return map;
            map = TmxParser.createMapFromTmx(path);

            this.loadMap(map);

            tiledMaps.put(path, map);
            return map;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void loadMap(TiledMap map) throws Exception {
        for (TileSet tileSet : map.getTileSets()) {
            for (TilesetTile tilesetTile : tileSet.getTilesetTiles()) {
                if (tilesetTile.getImage() != null) {
                    QuadMesh mesh = new QuadMesh();
                    mesh.getMaterial().getTextures().add(AssetsManager.get().getTexture(tilesetTile.getImage().getSource()));
                    tilesetTile.setMesh(mesh);
                }
            }
        }

        this.loadLayers(map, map);
    }

    private void loadLayers(TiledMap map, LayerHandler layerHandler) throws IOException {
        for (Layer layer : layerHandler.getLayers()) {
            if (layer instanceof TileLayer) {
                loadTileLayer(map, (TileLayer) layer);
            } else if (layer instanceof ObjectGroup) {
                loadObjectGroup(map, (ObjectGroup) layer);
            } else if (layer instanceof ImageLayer) {
                loadImageLayer(map, (ImageLayer) layer);
            } else if (layer instanceof Group) {
                loadGroup(map, (Group) layer);
            }

        }
    }

    private void loadTileLayer(TiledMap map, TileLayer tileLayer) throws IOException {
        Data data = tileLayer.getData();
        if (data.getRawContent() != null) {
            String dataContent = data.getRawContent();
            if (data.getEncoding().equals(Encoding.BASE64)) {
                byte[] bytes = DatatypeConverter.parseBase64Binary(dataContent);
                if (data.getCompression().equals(Compression.ZLIB)) {
                    bytes = decompressZlib(bytes, map.getHeight() * map.getWidth() * 4);

                    int tileIndex = 0;
                    for (int y = 0; y < map.getHeight(); ++y) {
                        for (int x = 0; x < map.getWidth(); ++x) {
                            int globalTileId = bytes[tileIndex] |
                                    bytes[tileIndex + 1] << 8 |
                                    bytes[tileIndex + 2] << 16 |
                                    bytes[tileIndex + 3] << 24;
                            tileIndex += 4;


                            int flippedHorizontally = (globalTileId & FLIPPED_HORIZONTALLY_FLAG);
                            int flippedVertically = (globalTileId & FLIPPED_VERTICALLY_FLAG);
                            int flippedDiagonally = (globalTileId & FLIPPED_DIAGONALLY_FLAG);
                            globalTileId &= ~(FLIPPED_HORIZONTALLY_FLAG | FLIPPED_VERTICALLY_FLAG | FLIPPED_DIAGONALLY_FLAG);

                            // Resolve the tile
                            for (int i = map.getTileSets().size() - 1; i >= 0; i--) {
                                TileSet tileset = map.getTileSets().get(i);

                                if (tileset.getFirstgid() <= globalTileId) {
                                    TilesetTile tilesetTile = tileset.getTilesetTiles().get(globalTileId - tileset.getFirstgid());

                                    RenderedTilesetTile tile = new RenderedTilesetTile();

                                    tile.setTileId(tilesetTile.getId());
                                    tile.setTileSetId(tilesetTile.getId());
                                    tile.setDiagonalFlipped(flippedDiagonally != 0);
                                    tile.setHorizontalFlipped(flippedHorizontally != 0);
                                    tile.setVerticalFlipped(flippedVertically != 0);

                                    tile.setPosition(convertMapCoordToInGameCoord(new Vector3f(x, y, -0.01f)));
                                    tile.setRenderer(new QuadRenderer(tilesetTile.getMesh()));

                                    tileLayer.getTiles().add(tile);
                                    break;
                                }
                            }
                        }
                    }
                } else {
                    //TODO
                }
            } else {
                //TODO
            }
        } else {
            //TODO
        }
    }

    private void loadObjectGroup(TiledMap map, ObjectGroup objectGroup) {
        //TODO
    }

    private void loadImageLayer(TiledMap map, ImageLayer imageLayer) {

    }

    private void loadGroup(TiledMap map, Group group) throws IOException {
        this.loadLayers(map, group);
    }

    public void cleanUp() {
        for (java.util.Map.Entry<String, Texture> entry : textures.entrySet()) {
            entry.getValue().cleanup();
        }
    }
}
