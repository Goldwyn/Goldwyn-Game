package fr.qilat.goldwyn.core.engine;

public interface IGameLogic {

    void init(Window window) throws Exception;

    void input(Window window) throws Exception;

    void update(Window window, float interval) throws Exception;

    void render(Window window);

    void cleanup();
}
