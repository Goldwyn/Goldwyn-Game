package fr.qilat.goldwyn.core.engine;

import fr.qilat.goldwyn.core.engine.item.GameItem;
import fr.qilat.goldwyn.core.engine.item.WorldBorder;
import fr.qilat.goldwyn.core.engine.tmx.TiledMap;
import lombok.Getter;
import lombok.Setter;

import org.dyn4j.collision.AxisAlignedBounds;
import org.dyn4j.dynamics.Body;
import org.dyn4j.geometry.Vector2;
import org.dyn4j.world.World;

import java.util.ArrayList;

/**
 * Created by Theophile on 2018-12-12 for projecty.
 */
public class Scene {

    @Getter
    private final ArrayList<GameItem> gameItems;

    @Getter
    @Setter
    private TiledMap tiledMap;

    private World<Body> world;

    public Scene() {
        gameItems = new ArrayList<>();
        this.world = new World<>();
    }

    public void init() throws Exception {
        this.world = new World<>();
        this.world.setGravity(new Vector2());

        this.gameItems.add(new WorldBorder(false, this.tiledMap.getWidth(), new Vector2(this.getTiledMap().getWidth()/2, 1.3))); //top
        this.gameItems.add(new WorldBorder(false, this.tiledMap.getWidth(), new Vector2(this.getTiledMap().getWidth()/2, -this.getTiledMap().getHeight() + 0.2))); //bottom
        this.gameItems.add(new WorldBorder(true, this.tiledMap.getHeight(), new Vector2(-0.95, -this.getTiledMap().getHeight()/2))); //left
        this.gameItems.add(new WorldBorder(true, this.tiledMap.getHeight(), new Vector2(this.getTiledMap().getWidth() + 0.05, -this.getTiledMap().getHeight()/2))); //right

        for (GameItem gameItem : this.gameItems) {
            gameItem.init(this.world);
        }
    }

    public void input() {
        for (GameItem gameItem : this.gameItems) {
            gameItem.input(this.world);
        }
    }

    public void update(float interval) {
        for (GameItem gameItem : this.gameItems) {
            gameItem.update(this.world, interval);
        }
        this.world.update(interval);
    }

    public void cleanup() {
        for (GameItem item : this.gameItems) {
            if(item.isVisible())
                item.getRenderer().getMesh().cleanUp();
        }
    }
    
    public void addGameItem(GameItem gameItem) {
        this.gameItems.add(gameItem);
    }
}
