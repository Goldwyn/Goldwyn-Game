package fr.qilat.goldwyn.core.engine.tmx;

import fr.qilat.goldwyn.core.engine.tmx.tileset.TileSet;
import lombok.Getter;
import lombok.Setter;
import org.joml.Vector4i;

import java.util.ArrayList;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public class TiledMap implements LayerHandler {

    @Getter
    @Setter
    private String version;
    @Getter
    @Setter
    private String tiledVersion;
    @Getter
    @Setter
    private Orientation orientation;
    @Getter
    @Setter
    private RenderOrder renderOrder;
    @Getter
    @Setter
    private int width;
    @Getter
    @Setter
    private int height;
    @Getter
    @Setter
    private int tileWidth;
    @Getter
    @Setter
    private int tileHeight;
    @Getter
    @Setter
    private int hexSideLength;
    @Getter
    @Setter
    private StaggerAxis staggerAxis;
    @Getter
    @Setter
    private String staggerIndex;
    @Getter
    @Setter
    private Vector4i backgroundColor;
    @Getter
    @Setter
    private int nextLayerId;
    @Getter
    @Setter
    private int nextObjectId;

    @Getter
    private ArrayList<Layer> layers = new ArrayList<>();
    @Getter
    @Setter
    private Properties properties = new Properties();
    @Getter
    private ArrayList<TileSet> tileSets = new ArrayList<>();
    @Getter
    private ArrayList<Group> groups = new ArrayList<>();
}
