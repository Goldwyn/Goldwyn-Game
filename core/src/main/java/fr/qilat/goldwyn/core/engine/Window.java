package fr.qilat.goldwyn.core.engine;

import fr.qilat.goldwyn.core.engine.input.Input;
import lombok.Getter;
import lombok.Setter;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import org.lwjgl.system.MemoryStack;

import java.nio.IntBuffer;

import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.system.MemoryUtil.NULL;

/**
 * Created by Qilat on 2019-03-27 for goldwyn.
 */
public class Window {

    @Getter
    private long glfwId = -1L;
    @Getter
    private String title;
    @Getter
    private int width;
    @Getter
    private int height;
    @Getter
    private boolean vsync;
    @Getter
    private boolean resizable;
    @Getter
    @Setter
    private boolean resized;
    @Getter
    private boolean closed;
    @Getter
    private boolean fullscreen = false;

    Window(String title, int width, int height, boolean vsync, boolean resizable) {
        this.title = title;

        this.width = width;
        this.height = height;

        this.vsync = vsync;
        this.resizable = resizable;
    }

    void init() {
        GLFWErrorCallback.createPrint(System.err).set();

        if (!glfwInit()) {
            throw new IllegalStateException("Unable to initialize GLFW");
        }

        glfwDefaultWindowHints();
        glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
        glfwWindowHint(GLFW_RESIZABLE, resizable ? GL_TRUE : GL_FALSE);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

        if (width == 0 || height == 0) {
            GLFWVidMode vidMode = glfwGetVideoMode(glfwGetPrimaryMonitor());
            if (vidMode != null) {
                width = vidMode.width();
                height = vidMode.height();
            }
        }

        if (this.fullscreen) {
            glfwWindowHint(GLFW_DECORATED, GLFW_FALSE);
        }

        glfwId = glfwCreateWindow(width, height, title, fullscreen ? glfwGetPrimaryMonitor() : NULL, glfwId == -1L ? NULL : glfwId);

        if (glfwId == NULL) {
            throw new RuntimeException("Failed to create the GLFW window");
        }

        glfwSetFramebufferSizeCallback(glfwId, (window, width, height) -> {
            this.width = width;
            this.height = height;
            this.setResized(true);
        });

        glfwSetKeyCallback(glfwId, Input.keyboard);
        glfwSetMouseButtonCallback(glfwId, Input.mouse);
        glfwSetScrollCallback(glfwId, Input.scroll);

        centerWindowOnScreen();

        glfwMakeContextCurrent(glfwId);

        if (isVsync()) {
            glfwSwapInterval(1);
        }

        glfwShowWindow(glfwId);

        GL.createCapabilities();
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        glEnable(GL_DEPTH_TEST);

        // Support for transparencies
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        //Remove unvisible triangle from render
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
    }

    void update() {
        glfwSwapBuffers(glfwId);
        Input.update();
    }

    void destroy() {
        glfwFreeCallbacks(glfwId);
        glfwDestroyWindow(glfwId);
    }

    public void setFullscreen(boolean fullscreen) {
        this.fullscreen = fullscreen;
        long oldGlfwInt = this.glfwId;
        this.init();
        glfwSetWindowShouldClose(oldGlfwInt, true);
        glfwFreeCallbacks(oldGlfwInt);
        glfwDestroyWindow(oldGlfwInt);
    }

    public void setVisible(boolean visible) {
        if (visible)
            glfwShowWindow(glfwId);
        else
            glfwHideWindow(glfwId);
    }

    public void setClearColor(float r, float g, float b, float alpha) {
        glClearColor(r, g, b, alpha);
    }

    private int[] getWindowSize() {
        MemoryStack stack = stackPush();
        IntBuffer pWidth = stack.mallocInt(1);
        IntBuffer pHeight = stack.mallocInt(1);

        glfwGetWindowSize(glfwId, pWidth, pHeight);
        return new int[]{pWidth.get(0), pHeight.get(0)};
    }

    private void centerWindowOnScreen() {
        int[] dim = getWindowSize();
        GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
        if (vidmode != null)
            glfwSetWindowPos(
                    this.glfwId,
                    (vidmode.width() - dim[0]) / 2,
                    (vidmode.height() - dim[1]) / 2
            );
    }

    public boolean shouldWindowClose() {
        return glfwWindowShouldClose(this.glfwId);
    }

    public void closeWindow() {
        glfwSetWindowShouldClose(glfwId, true);
        this.closed = true;
    }

}
