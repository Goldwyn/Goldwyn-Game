package fr.qilat.goldwyn.core.engine.tmx.objectgroup;

import fr.qilat.goldwyn.core.engine.tmx.Dimensionable;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public class Ellipse extends Shape implements Dimensionable {

    @Getter
    @Setter
    private int x;
    @Getter
    @Setter
    private int y;
    @Getter
    @Setter
    private int width;
    @Getter
    @Setter
    private int height;

}
