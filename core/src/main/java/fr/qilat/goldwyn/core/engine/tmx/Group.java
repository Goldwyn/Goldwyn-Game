package fr.qilat.goldwyn.core.engine.tmx;


import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public class Group extends Layer implements LayerHandler {

    @Getter
    @Setter
    private Properties properties;

    @Getter
    private ArrayList<Layer> layers = new ArrayList<>();
    @Getter
    private ArrayList<Group> groups = new ArrayList<>();
}
