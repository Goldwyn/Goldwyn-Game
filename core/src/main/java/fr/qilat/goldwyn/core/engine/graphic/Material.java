package fr.qilat.goldwyn.core.engine.graphic;

import lombok.Getter;
import lombok.Setter;
import org.joml.Vector4f;

import java.util.ArrayList;

public class Material {

    private static final Vector4f DEFAULT_COLOUR = new Vector4f(1.0f, 1.0f, 1.0f, 1.0f);

    @Getter
    @Setter
    private ArrayList<Texture> textures;

    @Getter
    @Setter
    private Vector4f ambientColour = DEFAULT_COLOUR;

    public Material() {
        this.textures = new ArrayList<>();
    }

    public Material(Texture texture) {
        this();
        this.textures.add(texture);
    }

    public boolean isTextured() {
        return this.textures.size() > 0 && this.textures.get(0) != null;
    }

    /**
     * Add new texture to this
     *
     * @param texture item to add
     * @return id in textures list
     */
    private int addTexture(Texture texture) {
        textures.add(texture);
        return textures.size() - 1;
    }

}
