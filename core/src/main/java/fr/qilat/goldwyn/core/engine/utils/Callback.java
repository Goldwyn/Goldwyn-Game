package fr.qilat.goldwyn.core.engine.utils;

/**
 * Created by Qilat on 2019-03-29 for goldwyn.
 */
public interface Callback {
    void call();
}
