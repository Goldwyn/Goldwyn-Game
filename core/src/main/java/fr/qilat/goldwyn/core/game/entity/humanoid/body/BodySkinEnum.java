package fr.qilat.goldwyn.core.game.entity.humanoid.body;

import lombok.Getter;

/**
 * Created by Qilat on 2019-03-28 for goldwyn.
 */
public class BodySkinEnum {

    public static final String TEXTURE_ROOT_PATH = "/textures/humanoid";
    public static final String EMPTY_DEFAULT_PATH = TEXTURE_ROOT_PATH + "/default.png";

    public static final String SEX_PATTERN = "{sex}";
    public static final String RACE_PATTERN = "{race}";
    public static final String BASE_BODY_PATH = TEXTURE_ROOT_PATH + "/body/" + SEX_PATTERN + "/" + RACE_PATTERN + ".png";
    public static final String EARS_PATTERN = "{ears}";
    public static final String EARS_PATH = TEXTURE_ROOT_PATH + "/body/" + SEX_PATTERN + "/ears/" + EARS_PATTERN + ".png";
    public static final String EYES_PATTERN = "{eyes}";
    public static final String EYES_PATH = TEXTURE_ROOT_PATH + "/body/" + SEX_PATTERN + "/eyes/" + EYES_PATTERN + ".png";
    public static final String NOSE_PATTERN = "{nose}";
    public static final String NOSE_PATH = TEXTURE_ROOT_PATH + "/body/" + SEX_PATTERN + "/nose/" + NOSE_PATTERN + ".png";

    public enum Race {
        DARK(0, "dark"),
        DARK2(1, "dark2"),
        DARKELF(2, "darkelf"),
        DARKELF2(3, "darkelf2"),
        LIGHT(4, "light"),
        ORC(5, "orc"),
        REDORC(6, "red_orc"),
        SKELETON(7, "skeleton"),
        TANNED(8, "tanned"),
        TANNED2(9, "tanned2");

        @Getter
        private int id;
        @Getter
        private String path;

        Race(int id, String path) {
            this.id = id;
            this.path = path;
        }
    }

    public enum Sex {
        MALE(0, "male"),
        FEMALE(1, "female");

        @Getter
        private int id;
        @Getter
        private String path;

        Sex(int id, String path) {
            this.id = id;
            this.path = path;
        }
    }

    public enum Ears {
        DEFAULT(0, null),
        BIG(1, "bigears_" + RACE_PATTERN),
        ELVEN(2, "elvenears_" + RACE_PATTERN);

        @Getter
        private int id;
        @Getter
        private String path;

        Ears(int id, String path) {
            this.id = id;
            this.path = path;
        }
    }

    public enum Eyes {
        DEFAULT(0, null),
        BLUE(1, "blue"),
        BROWN(2, "brown"),
        GRAY(3, "gray"),
        GREEN(4, "green"),
        ORANGE(5, "orange"),
        PURPLE(6, "purple"),
        RED(7, "red"),
        YELLOW(8, "yellow"),
        SKELETON(9, "casting_eyeglow_skeleton");

        @Getter
        private int id;
        @Getter
        private String path;

        Eyes(int id, String path) {
            this.id = id;
            this.path = path;
        }
    }

    public enum Nose {
        DEFAULT(0, null),
        BIG(1, "bignose_" + RACE_PATTERN),
        BUTTON(2, "buttonnose_" + RACE_PATTERN),
        STRAIGHT(3, "straightnose_" + RACE_PATTERN);

        @Getter
        private int id;
        @Getter
        private String path;

        Nose(int id, String path) {
            this.id = id;
            this.path = path;
        }
    }
}
