package fr.qilat.goldwyn.core.engine.tmx.objectgroup;

import lombok.Getter;
import lombok.Setter;
import org.joml.Vector4i;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public class Text extends Shape {

    @Getter
    @Setter
    private String fontFamily;
    @Getter
    @Setter
    private int pixelSize;
    @Getter
    @Setter
    private boolean wrap;
    @Getter
    @Setter
    private Vector4i color;
    @Getter
    @Setter
    private boolean bold;
    @Getter
    @Setter
    private boolean italic;
    @Getter
    @Setter
    private boolean underline;
    @Getter
    @Setter
    private boolean strikeout;
    @Getter
    @Setter
    private boolean kerning;
    @Getter
    @Setter
    private HAlign hAlign;
    @Getter
    @Setter
    private VAlign vAlign;

    @Getter
    @Setter
    private String content;

}
