package fr.qilat.goldwyn.core.engine.item;

import fr.qilat.goldwyn.core.engine.graphic.Mesh;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public abstract class GameItemRenderer {

    @Getter
    @Setter
    private Mesh mesh;

    public GameItemRenderer(Mesh mesh) {
        this.mesh = mesh;
    }

    public abstract void update(float interval);

}
