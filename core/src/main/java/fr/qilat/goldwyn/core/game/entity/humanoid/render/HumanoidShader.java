package fr.qilat.goldwyn.core.game.entity.humanoid.render;

import fr.qilat.goldwyn.core.engine.graphic.ShaderProgram;
import fr.qilat.goldwyn.core.engine.graphic.Transformation;
import fr.qilat.goldwyn.core.engine.utils.Utils;
import fr.qilat.goldwyn.core.game.entity.humanoid.Humanoid;
import org.joml.Matrix4f;

import java.util.List;

/**
 * Created by Qilat on 2019-03-29 for goldwyn.
 */
public class HumanoidShader extends ShaderProgram {


    public HumanoidShader() throws Exception {
        super();
        this.createVertexShader(Utils.loadResource("/shaders/humanoid_vertex.glsl"));
        this.createFragmentShader(Utils.loadResource("/shaders/humanoid_fragment.glsl"));
        this.link();

        // Create uniforms for modelView and projection matrices and texture
        this.createUniform("modelViewMatrix");
        this.createUniform("projectionMatrix");

        // Create humanoidShaderProgram for material
        this.createUniform("hasTexture");
        this.createUniform("baseTexture");
        this.createUniform("earsTexture");
        this.createUniform("eyesTexture");
        this.createUniform("noseTexture");
        this.createUniform("colour");
    }


    public void render(Transformation transformation, List<Humanoid> humanoids) {
        this.bind();
        this.setUniform("projectionMatrix", transformation.getProjectionMatrix());
        Matrix4f viewMatrix = transformation.getViewMatrix();
        for (Humanoid humanoid : humanoids) {
            render(transformation, viewMatrix, humanoid);
        }
        this.unbind();
    }

    public void render(Transformation transformation, Matrix4f viewMatrix, Humanoid humanoid) {
        this.setUniform("hasTexture", humanoid.getRenderer().getMesh().getMaterial().isTextured() ? 1 : 0);
        this.setUniform("colour", humanoid.getRenderer().getMesh().getMaterial().getAmbientColour());
        this.setUniform("baseTexture", 0);
        this.setUniform("earsTexture", 1);
        this.setUniform("eyesTexture", 2);
        this.setUniform("noseTexture", 3);

        Matrix4f modelViewMatrix = transformation.buildModelViewMatrix(humanoid, viewMatrix);
        this.setUniform("modelViewMatrix", modelViewMatrix);

        humanoid.getRenderer().getMesh().render();
    }

}
