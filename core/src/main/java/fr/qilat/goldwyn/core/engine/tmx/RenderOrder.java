package fr.qilat.goldwyn.core.engine.tmx;

import lombok.Getter;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public enum RenderOrder {

    RIGHTDOWN("right-down"),
    RIGHTUP("right-up"),
    LEFTDOWN("left-down"),
    LEFTUP("left-up");


    @Getter
    private String xmlValue;

    RenderOrder(String xmlValue) {
        this.xmlValue = xmlValue;
    }

}
