package fr.qilat.goldwyn.core.engine.tmx.tileset.tile;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public class Animation {

    @Getter
    @Setter
    ArrayList<Frame> frames = new ArrayList<>();

}
