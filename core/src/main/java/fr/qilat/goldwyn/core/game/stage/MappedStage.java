package fr.qilat.goldwyn.core.game.stage;

import fr.qilat.goldwyn.core.engine.AssetsManager;
import fr.qilat.goldwyn.core.engine.IGameLogic;
import fr.qilat.goldwyn.core.engine.Scene;
import fr.qilat.goldwyn.core.engine.Window;
import fr.qilat.goldwyn.core.engine.graphic.Camera;
import fr.qilat.goldwyn.core.engine.graphic.IRenderer;
import fr.qilat.goldwyn.core.engine.input.Input;
import fr.qilat.goldwyn.core.engine.item.GameItem;
import fr.qilat.goldwyn.core.game.Hud;
import fr.qilat.goldwyn.core.game.entity.humanoid.player.Player;

import org.joml.Vector3f;

/**
 * Created by Qilat on 2019-03-27 for goldwyn.
 */
public class MappedStage implements IGameLogic {

    private final IRenderer renderer;
    private final Camera camera;
    private final Vector3f cameraPositionOffset;

    private final Scene scene;
    private final Hud hud;

    private Player player;

    public MappedStage() {
        renderer = new MappedStageRenderer();
        camera = new Camera();
        cameraPositionOffset = new Vector3f(0.0f, 0.0f, 0.0f);
        scene = new Scene();
        hud = new Hud();
    }

    @Override
    public void init(Window window) throws Exception {
        this.player = new Player();
        this.player.setPosition(new Vector3f(5.0f, -5.0f, 0.0f));
        this.player.setScale(2.5f);

        this.scene.addGameItem(this.player);
        this.scene.setTiledMap(AssetsManager.get().getTiledMap("map/home.tmx"));
        this.scene.init();
        
        this.renderer.init(window);
    }

    @Override
    public void input(Window window) {
        this.scene.input();

        this.cameraPositionOffset.set(0, 0, 0);
        if (Input.getLastYOffset() > 0) {
            this.cameraPositionOffset.z = -1;
        } else if (Input.getLastYOffset() < 0) {
            this.cameraPositionOffset.z = 1;
        }

    }

    @Override
    public void update(Window window, float interval) {
        this.scene.update(interval);
        
        this.camera.clampPosition((float) this.player.getPosition().x, (float) this.player.getPosition().y, (this.camera.getPosition().z + this.cameraPositionOffset.z));

        for (GameItem gameItem : this.scene.getGameItems()) {
            if(gameItem.isVisible())
                gameItem.getRenderer().update(interval);
        }
    }

    @Override
    public void render(Window window) {
        this.renderer.render(window, this.camera, this.scene, this.hud);
    }

    @Override
    public void cleanup() {
        this.renderer.cleanup();
        this.scene.cleanup();
        this.hud.cleanup();
    }
}
