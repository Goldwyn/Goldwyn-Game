package fr.qilat.goldwyn.core.game.entity.humanoid.player;

import org.dyn4j.dynamics.Body;
import org.dyn4j.world.World;

import fr.qilat.goldwyn.core.engine.input.Controls;
import fr.qilat.goldwyn.core.engine.input.Input;
import fr.qilat.goldwyn.core.game.Data;
import fr.qilat.goldwyn.core.game.entity.humanoid.Humanoid;
import fr.qilat.goldwyn.core.game.entity.humanoid.animation.AnimationEnum;
import fr.qilat.goldwyn.core.game.entity.humanoid.body.BodySkinEnum;

/**
 * Created by Qilat on 2019-03-27 for goldwyn.
 */
public class Player extends Humanoid {

    public Player() {
        super();
        this.setVisible(true);
        this.getBodySkin().setRace(BodySkinEnum.Race.DARK);
        this.getBodySkin().setSex(BodySkinEnum.Sex.FEMALE);
        this.getBodySkin().setEars(BodySkinEnum.Ears.BIG);
        this.getBodySkin().setEyes(BodySkinEnum.Eyes.RED);
        this.getBodySkin().setNose(BodySkinEnum.Nose.DEFAULT);
    }

    @Override
    public void input(World<Body> world) {
        if (Input.keyDown(Controls.WALK_LEFT)) {
            this.getPhysicBody().getLinearVelocity().x= -Data.WALK_SPEED;
            this.getPhysicBody().setAtRest(false);
            this.getState().setFacing(AnimationEnum.Facing.LEFT);
        } else if (Input.keyDown(Controls.WALK_RIGHT)) {
            this.getPhysicBody().getLinearVelocity().x = Data.WALK_SPEED;
            this.getPhysicBody().setAtRest(false);
            this.getState().setFacing(AnimationEnum.Facing.RIGHT);
        } else {
            this.getPhysicBody().getLinearVelocity().x = 0;
            this.getPhysicBody().setAtRest(false);
        }

        if (Input.keyDown(Controls.WALK_UP)) {
            this.getPhysicBody().getLinearVelocity().y = Data.WALK_SPEED;
            this.getPhysicBody().setAtRest(false);
            this.getState().setFacing(AnimationEnum.Facing.TOP);
        } else if (Input.keyDown(Controls.WALK_DOWN)) {
            this.getPhysicBody().getLinearVelocity().y = -Data.WALK_SPEED;
            this.getPhysicBody().setAtRest(false);
            this.getState().setFacing(AnimationEnum.Facing.BOTTOM);
        } else {
            this.getPhysicBody().getLinearVelocity().y = 0;
            this.getPhysicBody().setAtRest(false);
        }
    }

}
