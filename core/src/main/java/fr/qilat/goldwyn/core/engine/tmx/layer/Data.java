package fr.qilat.goldwyn.core.engine.tmx.layer;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public class Data {

    @Getter
    @Setter
    private Encoding encoding;
    @Getter
    @Setter
    private Compression compression;
    @Getter
    @Setter
    private String rawContent;
    @Getter
    @Setter
    private ArrayList<Chunk> chunks = new ArrayList<>();
    @Getter
    @Setter
    private ArrayList<Tile> tiles = new ArrayList<>();

}
