package fr.qilat.goldwyn.core.engine.tmx;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public class Property {

    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private PropertyType type;
    @Getter
    @Setter
    private String value;

}
