package fr.qilat.goldwyn.core.game;

import fr.qilat.goldwyn.core.engine.IHud;
import fr.qilat.goldwyn.core.engine.item.GameItem;

/**
 * Created by Qilat on 2019-03-27 for goldwyn.
 */
public class Hud implements IHud {
    @Override
    public GameItem[] getGameItems() {
        return new GameItem[0];
    }

    @Override
    public void cleanup() {

    }
}
