package fr.qilat.goldwyn.core.engine.tmx.tileset;

import fr.qilat.goldwyn.core.engine.tmx.layer.Data;
import fr.qilat.goldwyn.core.engine.tmx.objectgroup.Shape;
import fr.qilat.goldwyn.core.game.map.QuadMesh;
import lombok.Getter;
import lombok.Setter;
import org.joml.Vector4i;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public class Image extends Shape {

    @Getter
    @Setter
    private String format;
    @Getter
    @Setter
    private int id;
    @Getter
    @Setter
    private String source;
    @Getter
    @Setter
    private Vector4i trans;
    @Getter
    @Setter
    private int width;
    @Getter
    @Setter
    private int height;

    @Getter
    @Setter
    private Data data;

    @Getter
    @Setter
    private QuadMesh mesh;
}
