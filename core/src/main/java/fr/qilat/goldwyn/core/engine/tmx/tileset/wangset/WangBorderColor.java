package fr.qilat.goldwyn.core.engine.tmx.tileset.wangset;

import lombok.Getter;
import lombok.Setter;
import org.joml.Vector4i;

/**
 * Created by Qilat on 2019-03-31 for goldwyn.
 */
public abstract class WangBorderColor {

    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private Vector4i color;
    @Getter
    @Setter
    private int tile;
    @Getter
    @Setter
    private float probability;

}
