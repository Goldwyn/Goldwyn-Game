package fr.qilat.goldwyn.core.game.entity.humanoid.body;

import lombok.Getter;
import lombok.Setter;


/**
 * Created by Qilat on 2019-03-28 for goldwyn.
 */
public class BodySkin {

    @Getter
    private BodySkinEnum.Sex sex = BodySkinEnum.Sex.MALE;
    @Getter
    private BodySkinEnum.Race race = BodySkinEnum.Race.DARK;
    @Getter
    private BodySkinEnum.Ears ears = BodySkinEnum.Ears.DEFAULT;
    @Getter
    private BodySkinEnum.Eyes eyes = BodySkinEnum.Eyes.DEFAULT;
    @Getter
    private BodySkinEnum.Nose nose = BodySkinEnum.Nose.DEFAULT;

    @Getter
    @Setter
    private boolean sexChanging = true;
    @Getter
    @Setter
    private boolean raceChanging = true;
    @Getter
    @Setter
    private boolean earsChanging = true;
    @Getter
    @Setter
    private boolean eyesChanging = true;
    @Getter
    @Setter
    private boolean noseChanging = true;

    public BodySkin() {
    }

    public BodySkin(BodySkinEnum.Sex sex, BodySkinEnum.Race race, BodySkinEnum.Ears ears) {
        this.setSex(sex);
        this.setRace(race);
        this.setEars(ears);
    }

    public void setSex(BodySkinEnum.Sex sex) {
        if (this.sex != sex) {
            sexChanging = true;
            if (race == BodySkinEnum.Race.SKELETON) {
                this.sex = BodySkinEnum.Sex.MALE;
            } else {
                this.sex = sex;
            }
            updateBody();
        }
    }

    public void setRace(BodySkinEnum.Race race) {
        if (this.race != race) {
            raceChanging = true;
            this.race = race;

            if (this.race == BodySkinEnum.Race.SKELETON) {
                this.sex = BodySkinEnum.Sex.MALE;
            }

            updateBody();
        }
    }

    @SuppressWarnings("Duplicates")
    public void setEars(BodySkinEnum.Ears ears) {
        if (this.ears != ears || sexChanging || raceChanging) {
            earsChanging = true;

            if (race == BodySkinEnum.Race.DARK
                    || race == BodySkinEnum.Race.DARK2
                    || race == BodySkinEnum.Race.DARKELF
                    || race == BodySkinEnum.Race.DARKELF2
                    || race == BodySkinEnum.Race.LIGHT
                    || race == BodySkinEnum.Race.TANNED
                    || race == BodySkinEnum.Race.TANNED2) {
                this.ears = ears;
            } else {
                this.ears = BodySkinEnum.Ears.DEFAULT;
            }
        }
    }

    @SuppressWarnings("Duplicates")
    public void setNose(BodySkinEnum.Nose nose) {
        if (this.nose != nose || sexChanging || raceChanging) {
            noseChanging = true;

            if (race == BodySkinEnum.Race.DARK
                    || race == BodySkinEnum.Race.DARK2
                    || race == BodySkinEnum.Race.DARKELF
                    || race == BodySkinEnum.Race.DARKELF2
                    || race == BodySkinEnum.Race.LIGHT
                    || race == BodySkinEnum.Race.TANNED
                    || race == BodySkinEnum.Race.TANNED2) {
                this.nose = nose;
            } else {
                this.nose = BodySkinEnum.Nose.DEFAULT;
            }
        }
    }

    public void setEyes(BodySkinEnum.Eyes eyes) {
        if (this.eyes != eyes || sexChanging || raceChanging) {
            eyesChanging = true;
            if (race == BodySkinEnum.Race.SKELETON) {
                this.eyes = BodySkinEnum.Eyes.SKELETON;
            } else {
                this.eyes = eyes;
            }
        }
    }

    private void updateBody() {
        setEars(getEars());
        setNose(getNose());
        setEyes(getEyes());
    }

    public boolean isChanging() {
        return this.sexChanging
                || this.raceChanging
                || this.earsChanging
                || this.eyesChanging
                || this.noseChanging;
    }
}
