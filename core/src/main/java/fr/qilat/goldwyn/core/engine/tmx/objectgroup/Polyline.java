package fr.qilat.goldwyn.core.engine.tmx.objectgroup;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public class Polyline extends Shape {

    @Getter
    @Setter
    private ArrayList<Point> points = new ArrayList<>();

}
