package fr.qilat.goldwyn.core.engine;

import fr.qilat.goldwyn.core.engine.item.GameItem;

/**
 * Created by Theophile on 2018-12-11 for ProjectY.
 */
public interface IHud {

    GameItem[] getGameItems();

    default void cleanup() {
        GameItem[] gameItems = getGameItems();
        for (GameItem gameItem : gameItems) {
            gameItem.getRenderer().getMesh().cleanUp();
        }
    }
}
