package fr.qilat.goldwyn.core.engine.tmx;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public abstract class Layer {

    @Getter
    @Setter
    private int id;
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private int offsetX = 0;
    @Getter
    @Setter
    private int offsetY = 0;
    @Getter
    @Setter
    private float opacity = 1.0f;
    @Getter
    @Setter
    private boolean visible = true;

    @Getter
    @Setter
    private Properties properties;
}
