package fr.qilat.goldwyn.core.engine.tmx;

import java.util.List;

/**
 * Created by Qilat on 2019-06-19 for goldwyn.
 */
public interface LayerHandler {

    List<Layer> getLayers();

    List<Group> getGroups();
}
