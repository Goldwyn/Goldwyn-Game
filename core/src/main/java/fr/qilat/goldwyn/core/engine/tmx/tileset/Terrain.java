package fr.qilat.goldwyn.core.engine.tmx.tileset;

import fr.qilat.goldwyn.core.engine.tmx.Properties;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public class Terrain {

    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private int tile;
    @Getter
    @Setter
    private Properties properties;

}
