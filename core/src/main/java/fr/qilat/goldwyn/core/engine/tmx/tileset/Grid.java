package fr.qilat.goldwyn.core.engine.tmx.tileset;

import fr.qilat.goldwyn.core.engine.tmx.Orientation;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public class Grid {

    @Getter
    @Setter
    private Orientation orientation;
    @Getter
    @Setter
    private int width;
    @Getter
    @Setter
    private int height;


}
