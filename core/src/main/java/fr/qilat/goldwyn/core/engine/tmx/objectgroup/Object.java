package fr.qilat.goldwyn.core.engine.tmx.objectgroup;

import fr.qilat.goldwyn.core.engine.tmx.Dimensionable;
import fr.qilat.goldwyn.core.engine.tmx.Properties;
import fr.qilat.goldwyn.core.engine.tmx.Template;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public class Object implements Dimensionable {

    @Getter
    @Setter
    private int id;
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private String type;
    @Getter
    @Setter
    private int x;
    @Getter
    @Setter
    private int y;
    @Getter
    @Setter
    private int width;
    @Getter
    @Setter
    private int height;
    @Getter
    @Setter
    private float rotation;
    @Getter
    @Setter
    private int gid;
    @Getter
    @Setter
    private boolean visible;
    @Getter
    @Setter
    private Template template;

    @Getter
    @Setter
    private Properties properties;
    @Getter
    @Setter
    private Shape shape;

    public Object() {
    }

    public Object(Object object) {
        this.id = object.getId();
        this.name = object.getName();
        this.type = object.getType();
        this.x = object.getX();
        this.y = object.getY();
        this.width = object.getWidth();
        this.height = object.getHeight();
        this.rotation = object.getRotation();
        this.gid = object.getGid();
        this.visible = object.isVisible();
        this.properties = object.getProperties();
        this.shape = object.getShape();
    }
}
