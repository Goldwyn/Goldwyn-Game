package fr.qilat.goldwyn.core.engine.tmx.layer;

import org.dyn4j.dynamics.Body;
import org.dyn4j.world.World;

import fr.qilat.goldwyn.core.engine.item.GameItem;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Qilat on 2019-03-31 for goldwyn.
 */
public class RenderedTilesetTile extends GameItem {

    @Getter
    @Setter
    private int tileSetId;
    @Getter
    @Setter
    private int tileId;
    @Getter
    @Setter
    private boolean horizontalFlipped;
    @Getter
    @Setter
    private boolean verticalFlipped;
    @Getter
    @Setter
    private boolean diagonalFlipped;


    public RenderedTilesetTile() {
        super();
    }


    @Override
    public void init(World<Body> world) {
        // TODO Auto-generated method stub
        
    }


    @Override
    public void input(World<Body> world) {
        // TODO Auto-generated method stub
        
    }


    @Override
    public void update(World<Body> world, float interval) {
        // TODO Auto-generated method stub
        
    }
}
