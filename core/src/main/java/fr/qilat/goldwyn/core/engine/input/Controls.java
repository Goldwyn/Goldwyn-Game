package fr.qilat.goldwyn.core.engine.input;

import static org.lwjgl.glfw.GLFW.*;

public class Controls {

    public static int WALK_DOWN = GLFW_KEY_S;
    public static int WALK_UP = GLFW_KEY_W;
    public static int WALK_RIGHT = GLFW_KEY_D;
    public static int WALK_LEFT = GLFW_KEY_A;

}
