package fr.qilat.goldwyn.core.engine.tmx;

import lombok.Getter;

import java.util.HashMap;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public class Properties {

    @Getter
    private HashMap<String, Property> properties = new HashMap<>();


    public void add(String name, Property property) {
        this.properties.put(name, property);
    }

    public void remove(String name) {
        this.properties.remove(name);
    }

    public Property get(String name) {
        return this.properties.get(name);
    }

    public int size() {
        return this.properties.size();
    }

}
