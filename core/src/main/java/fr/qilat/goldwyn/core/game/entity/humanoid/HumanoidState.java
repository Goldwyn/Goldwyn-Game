package fr.qilat.goldwyn.core.game.entity.humanoid;

import fr.qilat.goldwyn.core.game.entity.humanoid.animation.AnimationEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Qilat on 2019-03-29 for goldwyn.
 */
public class HumanoidState {

    @Getter
    @Setter
    private AnimationEnum.Action action;
    @Getter
    @Setter
    private AnimationEnum.Facing facing;

    public HumanoidState(AnimationEnum.Action action, AnimationEnum.Facing facing) {
        this.action = action;
        this.facing = facing;
    }
}
