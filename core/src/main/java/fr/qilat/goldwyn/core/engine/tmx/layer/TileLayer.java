package fr.qilat.goldwyn.core.engine.tmx.layer;

import fr.qilat.goldwyn.core.engine.tmx.Dimensionable;
import fr.qilat.goldwyn.core.engine.tmx.Layer;
import fr.qilat.goldwyn.core.engine.tmx.Properties;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public class TileLayer extends Layer implements Dimensionable {

    @Getter
    @Setter
    private int x;
    @Getter
    @Setter
    private int y;
    @Getter
    @Setter
    private int width;
    @Getter
    @Setter
    private int height;

    @Getter
    @Setter
    private Properties properties;
    @Getter
    @Setter
    private Data data;


    @Getter
    @Setter
    private ArrayList<RenderedTilesetTile> tiles = new ArrayList<>();

}
