package fr.qilat.goldwyn.core.engine.tmx.layer;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public class Tile {

    @Getter
    @Setter
    private int gid;
    @Getter
    @Setter
    private String content;

}
