package fr.qilat.goldwyn.core.game.stage;

import fr.qilat.goldwyn.core.engine.graphic.ShaderProgram;
import fr.qilat.goldwyn.core.engine.graphic.Transformation;
import fr.qilat.goldwyn.core.engine.tmx.Group;
import fr.qilat.goldwyn.core.engine.tmx.ImageLayer;
import fr.qilat.goldwyn.core.engine.tmx.Layer;
import fr.qilat.goldwyn.core.engine.tmx.TiledMap;
import fr.qilat.goldwyn.core.engine.tmx.layer.RenderedTilesetTile;
import fr.qilat.goldwyn.core.engine.tmx.layer.TileLayer;
import fr.qilat.goldwyn.core.engine.tmx.objectgroup.ObjectGroup;
import fr.qilat.goldwyn.core.engine.utils.Utils;
import org.joml.Matrix4f;
import org.joml.Vector4f;

/**
 * Created by Qilat on 2019-03-31 for goldwyn.
 */
public class MapShader extends ShaderProgram {

    public MapShader() throws Exception {
        super();
        this.createVertexShader(Utils.loadResource("/shaders/map_vertex.glsl"));
        this.createFragmentShader(Utils.loadResource("/shaders/map_fragment.glsl"));
        this.link();

        // Create uniforms for modelView and projection matrices and texture
        this.createUniform("modelViewMatrix");
        this.createUniform("projectionMatrix");

        this.createUniform("baseTexture");
        this.createUniform("colour");
    }


    public void render(Transformation transformation, TiledMap map) {
        this.bind();
        this.setUniform("projectionMatrix", transformation.getProjectionMatrix());
        Matrix4f viewMatrix = transformation.getViewMatrix();

        for (int i = map.getLayers().size() - 1; i >= 0; i--) {
            this.setUniform("colour", new Vector4f(1.0f, 1.0f, 1.0f, 1.0f));
            render(transformation, viewMatrix, map.getLayers().get(i));
        }

        this.unbind();
    }

    public void render(Transformation transformation, Matrix4f viewMatrix, Layer layer) {

        if (layer instanceof TileLayer) {
            TileLayer tileLayer = (TileLayer) layer;
            for (RenderedTilesetTile tile : tileLayer.getTiles()) {
                render(transformation, viewMatrix, tile);
            }
        } else if (layer instanceof ImageLayer) {

        } else if (layer instanceof ObjectGroup) {
            //TODO
        } else if (layer instanceof Group) {
            //TODO
        }


    }

    public void render(Transformation transformation, Matrix4f viewMatrix, RenderedTilesetTile tile) {
        this.setUniform("baseTexture", 0);
        Matrix4f modelViewMatrix = transformation.buildModelViewMatrix(tile, viewMatrix);
        this.setUniform("modelViewMatrix", modelViewMatrix);
        tile.getRenderer().getMesh().render();
    }

}
