package fr.qilat.goldwyn.core.game.entity.humanoid.render;

import fr.qilat.goldwyn.core.engine.AssetsManager;
import fr.qilat.goldwyn.core.engine.graphic.Texture;
import fr.qilat.goldwyn.core.engine.item.GameItemRenderer;
import fr.qilat.goldwyn.core.game.entity.humanoid.Humanoid;
import fr.qilat.goldwyn.core.game.entity.humanoid.animation.Animation;
import fr.qilat.goldwyn.core.game.entity.humanoid.body.BodySkin;
import lombok.Getter;
import org.joml.Vector2f;

import java.util.ArrayList;

import static fr.qilat.goldwyn.core.game.entity.humanoid.body.BodySkinEnum.*;

/**
 * Created by Qilat on 2019-03-29 for goldwyn.
 */
public class HumanoidRenderer extends GameItemRenderer {

    private Humanoid humanoid;
    private BodySkin bodykin;
    @Getter
    private Animation animation;

    public HumanoidRenderer(Humanoid humanoid) {
        super(new HumanoidMesh());
        this.humanoid = humanoid;
        this.bodykin = humanoid.getBodySkin();
        this.animation = new Animation(humanoid.getState().getAction(), humanoid.getState().getFacing());
        this.animation.runCycle();
        this.updateBodyTexture();
    }

    private void setTexture(int id, Texture texture) {
        ArrayList<Texture> textures = this.getMesh().getMaterial().getTextures();
        if (textures.size() > id) {
            textures.set(id, texture);
        } else {
            textures.add(AssetsManager.get().getTexture(EMPTY_DEFAULT_PATH));
            setTexture(id, texture);
        }
    }

    private void updateBodyTexture() {
        //UPDATE ORIENTATION AND ACTION OF THE HUMANOID
        this.getAnimation().setAction(this.humanoid.getState().getAction());
        this.getAnimation().setFacing(this.humanoid.getState().getFacing());

        if (bodykin.isChanging()) {
            //UPDATE BASE BODY TEXTURE
            if (bodykin.isRaceChanging() || bodykin.isSexChanging()) {
                bodykin.setRaceChanging(false);
                bodykin.setSexChanging(false);
                String baseBodyPath = BASE_BODY_PATH
                        .replace(SEX_PATTERN, bodykin.getSex().getPath())
                        .replace(RACE_PATTERN, bodykin.getRace().getPath());
                this.setTexture(0, AssetsManager.get().getTexture(baseBodyPath));
            }

            //UPDATE EARS TEXTURE
            if (bodykin.isEarsChanging()) {
                bodykin.setEarsChanging(false);
                if (bodykin.getEars().equals(Ears.DEFAULT)) {
                    this.setTexture(1, AssetsManager.get().getTexture(EMPTY_DEFAULT_PATH));
                } else {
                    String baseBodyPath = EARS_PATH
                            .replace(EARS_PATTERN, bodykin.getEars().getPath())
                            .replace(SEX_PATTERN, bodykin.getSex().getPath())
                            .replace(RACE_PATTERN, bodykin.getRace().getPath());
                    this.setTexture(1, AssetsManager.get().getTexture(baseBodyPath));
                }
            }

            //UPDATE EYES TEXTURE
            if (bodykin.isEyesChanging()) {
                bodykin.setEyesChanging(false);
                if (bodykin.getEyes().equals(Eyes.DEFAULT)) {
                    this.setTexture(2, AssetsManager.get().getTexture(EMPTY_DEFAULT_PATH));
                } else {
                    String baseBodyPath = EYES_PATH
                            .replace(EYES_PATTERN, bodykin.getEyes().getPath())
                            .replace(SEX_PATTERN, bodykin.getSex().getPath());
                    this.setTexture(2, AssetsManager.get().getTexture(baseBodyPath));
                }
            }

            //UPDATE NOSE TEXTURE
            if (bodykin.isNoseChanging()) {
                bodykin.setNoseChanging(false);
                if (bodykin.getNose().equals(Nose.DEFAULT)) {
                    this.setTexture(3, AssetsManager.get().getTexture(EMPTY_DEFAULT_PATH));
                } else {
                    String baseBodyPath = NOSE_PATH
                            .replace(NOSE_PATTERN, bodykin.getNose().getPath())
                            .replace(SEX_PATTERN, bodykin.getSex().getPath())
                            .replace(RACE_PATTERN, bodykin.getRace().getPath());
                    this.setTexture(3, AssetsManager.get().getTexture(baseBodyPath));
                }
            }
        }
    }

    private void updateTextureCoords(float interval) {
        animation.update(interval);
        Vector2f vector2f = animation.getFramePos(HumanoidTextureInfos.FRAME_WIDTH, HumanoidTextureInfos.FRAME_HEIGHT);
        this.getMesh().updateTextureCoord(vector2f.x, vector2f.y, HumanoidTextureInfos.FRAME_WIDTH, HumanoidTextureInfos.FRAME_HEIGHT);
    }

    @Override
    public void update(float interval) {
        this.updateBodyTexture();
        this.updateTextureCoords(interval);
    }

}
