package fr.qilat.goldwyn.core.engine.tmx.objectgroup;

import fr.qilat.goldwyn.core.engine.tmx.Layer;
import lombok.Getter;
import lombok.Setter;
import org.joml.Vector4i;

import java.util.ArrayList;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public class ObjectGroup extends Layer {

    @Getter
    @Setter
    private Vector4i color;
    @Getter
    @Setter
    private DrawOrder drawOrder;

    @Getter
    @Setter
    private ArrayList<Object> objects = new ArrayList<>();

}
