package fr.qilat.goldwyn.core.engine.tmx.tileset;

import fr.qilat.goldwyn.core.engine.tmx.Properties;
import fr.qilat.goldwyn.core.engine.tmx.tileset.tile.TilesetTile;
import fr.qilat.goldwyn.core.engine.tmx.tileset.wangset.Wangsets;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

/**
 * Created by Qilat on 2019-03-30 for goldwyn.
 */
public class TileSet {

    @Getter
    @Setter
    private int firstgid;
    @Getter
    @Setter
    private String source;
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private int tileWidth;
    @Getter
    @Setter
    private int tileHeight;
    @Getter
    @Setter
    private int spacing;
    @Getter
    @Setter
    private int margin;
    @Getter
    @Setter
    private int tilecount;
    @Getter
    @Setter
    private int columns;

    @Getter
    @Setter
    private TileOffset tileOffset;
    @Getter
    @Setter
    private Grid grid;
    @Getter
    @Setter
    private Properties properties;
    @Getter
    @Setter
    private ArrayList<TerrainTypes> terraintypes = new ArrayList<>();
    @Getter
    @Setter
    private ArrayList<Image> images = new ArrayList<>();
    @Getter
    @Setter
    private ArrayList<TilesetTile> tilesetTiles = new ArrayList<>();
    @Getter
    @Setter
    private Wangsets wangsets;

}
